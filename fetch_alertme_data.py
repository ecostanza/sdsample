# This file is part of sdsample
#
# sdsample is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# sdsample is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with sdsample.  If not, see <http://www.gnu.org/licenses/>.

# encoding: utf-8
'''
This example shows how to:
- login
- create a sensor
- create a channel for this sensor
- upload and retrive data for the sensor-channel

Create a sensor and a channel is something that should be done just one off,
when setting up the system. These operations can also be done via the admin
web UI (but it may be tedious to do that for several sensors).

This example assumes that a user with username example and password example exists
and a UserProfile for the user exists
These can be created from http://127.0.0.1:8000/admin/
Add the user first, then when adding the UserProfile select the 'example' user
from the dropdown menu and leave everything else blank
'''

import requests
import simplejson as json
from datetime import datetime, timedelta
from time import mktime
import sys
import socket

hostname = socket.gethostname()

DEPLOYMENT_HOSTS = ['hci', 'hci-store']
if hostname in DEPLOYMENT_HOSTS:
	sdstore_url = 'https://hci.ecs.soton.ac.uk/sdsample/'
	log_filename = '/srv/log/ambient_loader/fetch_alertme_data.log'
else:
	sdstore_url = 'http://127.0.0.1:8000/'
	log_filename = './fetch_alertme_data.log'

alertme_login_url = 'https://api.alertme.com/v5/login'
alertme_url = 'https://api.alertme.com/v5/users/'

from csv import reader
import logging
logger = logging.getLogger("fetch_alertme_data")
logger.setLevel(logging.INFO)
fh = logging.FileHandler(log_filename)
fh.setFormatter(logging.Formatter('%(levelname)s %(asctime)s %(message)s'))
logger.addHandler(fh)
#logger.addHandler(logging.StreamHandler())
logging.captureWarnings(True)

https_verify = True

def load_alertme_device(alertme_username, alertme_password,
					device_name, channel):
	
	alertme_data = {
				"username" : alertme_username,
				"password" : alertme_password,
				"caller" : "myself"}
	
	s = requests.Session()
	s.post(alertme_login_url, alertme_data, verify=https_verify)
	
	hubs_response = s.get(alertme_url + alertme_username + '/hubs/', verify=https_verify)
	
	hub_id = json.loads(hubs_response.text)[0]['id']
	
	devices_response = s.get(alertme_url + alertme_username + '/hubs/' + hub_id + '/devices/', verify=https_verify)
	
	all_devices = json.loads(devices_response.text)
	#logger.debug('all_devices:' + str(all_devices))

	# filter for buttons
	all_devices = [x for x in all_devices if x['name'] == device_name]
	
	if len(all_devices) != 1:
		raise Exception(device_name + ' not availale!')

	device = all_devices[0]
	
	# TODO: get frequency and units -- possible?
	
	return s, hub_id, device

def login_sd_store(username, password):
	### login
	login_url = sdstore_url + 'accounts/login/?next=/'
	s = requests.Session()
	# get the login page to get the CSRF cookie
	r = s.get(login_url, verify=https_verify)
	
	# apart from obvious username and password we need to pass the
	# CSRF token, used by django for security
	login_params = {
				'username': username,
			  	'password': password,
			  	'csrfmiddlewaretoken': r.cookies['csrftoken']
			  	}
	r = s.post(login_url, data=login_params, verify=https_verify)
	### we are now logged in
	
	return s

def do_create_sd_store_sensor(sensor, d):
	s = login_sd_store(sensor['username'],sensor['username'])
	
	### create a sensor
	# (note that this can also be done from the admin web UI)
	sensors_url = sdstore_url + 'sdstore/sensors/'
	
	# "mac" is supposed to be mac address but formatting is not enforced
	# so it can be any text, however needs to be unique
	sensor_params = {"mac": d['id'],
	                "name": d['name'],
	                "sensor_type": d['type']}
	
	r = s.post(sensors_url, data=sensor_params, verify=https_verify)
	# the response contains the ID of the newly created sensor
	open('response.html', 'w').write(r.text)
	sensor_id = int(r.text)
	### sensor creation done
	logger.debug('sd_store sensor creation:' + str(r))
	
	### add the channel
	channel_name = sensor['channel']
	channel_url = sdstore_url + 'sdstore/sensor/%d/%s/' % (sensor_id, channel_name)
	
	units = {'temperature': 'degree C', 'energy': 'kWh'}
	channel_params = {
			"unit": units[channel_name],
			"reading_frequency": 120 # in seconds
			}
	
	r = s.post(channel_url, data=channel_params, verify=https_verify)
	### channel added

def load_last_reading(sensor):
	s = login_sd_store(sensor['username'],sensor['username'])
	
	data_url = sdstore_url + 'sdstore/sensor/%s/%s/last-reading/' % (sensor['id'], sensor['channel'])
		
	r = s.get(data_url, verify=https_verify)
	logger.debug("load_last_reading: %s, %s" % (r, r.text) )
	try:
		last_reading = json.loads(r.text)
		return last_reading
	except json.JSONDecodeError:
		return {}

def post_sd_store_data(sensor, data):
	s = login_sd_store(sensor['username'],sensor['username'])
	
	data_url = sdstore_url + 'sdstore/sensor/%s/%s/data/' % (sensor['id'], sensor['channel'])
	
	fmt = lambda t: datetime.fromtimestamp(t).strftime('%a %b %d %H:%M:%S %Y')
	f_data = [{'timestamp': fmt(t), 'value': v} for (t, v) in data if v is not None]
	dataString = json.dumps(f_data)
	post_data = {"data": dataString, 'fix_gaps': True}
	
	r = s.post(data_url, post_data, verify=https_verify)
	if r.status_code != 200:
		logger.warn(str(post_data))
		logger.warn("post_sd_store_data: %s, %s" % (r, r.text) )

def create_sd_store_sensors(sensor_params):
	logger.info('creating sd_store sensors')
	for sensor in sensor_params:
		logger.info("%s, %s" % (sensor['username'], sensor['device_name']))
		# username,alertme_username,alertme_password,device_name,channel
		# get the sensor mac and channel frequency (?) from AlertMe
		_, _, d = load_alertme_device(sensor['alertme_username'], sensor['alertme_password'], sensor['device_name'], sensor['channel'])
		# try to create a sensor with this name and this channel
		# if the sensor is already there, just display a warning message
		logger.info('information from AlertMe loaded')
		
		# create sensor and channel
		do_create_sd_store_sensor(sensor, d)
		logger.info('sd_store channel creation: ' + str(r))
		

def load_alertme_data(alertme_username, s, device, channel, start, end):
	segments = []
	interval = 120


	if channel == 'energy':
		# We need to load an extra reading, so that diffs can be calculated.
		start = start - timedelta(seconds=interval)

	if (end - start) > timedelta(days=2):
		# break down the query into sub-queries of 2 days (this is because of a limitation on AlertMe)
		while start < end:
			tmp = start + timedelta(days=2)
			if tmp < end:
				segments.append((start, tmp))
			else:
				segments.append((start, end))
			start = tmp
	else:
		segments.append((start, end))
	
	timestamps = []
	values = []
	for (start, end) in segments:
		start_timestamp = str(int(mktime(start.timetuple())))
		end_timestamp = str(int(mktime(end.timetuple())))
		
		url = alertme_username + '/hubs/' + hub_id + '/devices/' + device['type'] + '/'
		url += device['id'] + '/channels/' + channel + '?start='
		url += start_timestamp + '&end=' + end_timestamp+ '&interval=' + str(interval) + '&operation=average'
		
		logger.debug(url)
		readings_response = s.get(alertme_url + url, verify=https_verify)
		data = json.loads(readings_response.text)
		if 'error' in data:
			if data['error']['reason'] == 'NO_DATA':
				logger.warning('no data from: ' + url)
				continue
		#{u'values': {u'average': [18.5, 18.5, 18.5, 18.5]}, u'start': 1391711160, u'interval': 120, u'end': 1391711520}
		values += data['values']['average']
		timestamps += range(int(data['start']), int(data['end']) + int(data['interval']), int(data['interval']))


	if channel == 'energy':
		if values[0] == None:
			logger.error("No previous sensor reading")
			return ()
		prev_value = values[0]
		for i in range(1, len(values)):
			if values[i] != None:
				stored_val = values[i]
				values[i] = values[i] - prev_value
				prev_value = stored_val
		del values[0]

	return zip(timestamps, values)


args = [x for x in sys.argv if x[0] != '-']
flags = [x[1:] for x in sys.argv if x[0] == '-']
flags = [x[1:] if x[0] == '-' else x for x in flags]

sensor_params = []

r = reader(open(args[1], 'r'))
# skip header
h = r.next()
for line in r:
	if len(line) == 0 or line[0][0] == '#':
		continue
	sensor_params.append(dict(zip(h,line)))

if 'setup' in flags:
	create_sd_store_sensors(sensor_params)

logger.info('loading data')
for sensor in sensor_params:
	logger.info('processing %s %s' % (sensor['username'], sensor['device_name']))
	# get the sensor details from alertme
	alertme_session, hub_id, d = load_alertme_device(sensor['alertme_username'], sensor['alertme_password'], sensor['device_name'], sensor['channel'])
	sensor['id'] = d['id']
	
	end = datetime.now()
	# get the last timestamp for this sensor in sd_store
	last_reading = load_last_reading(sensor)
	# set the start to the latest timestamp in sd_store
	try:
		logger.info('last timestamp: ' + str(last_reading['timestamp']))
		start = last_reading['timestamp']
		start = datetime.strptime(start, '%Y-%m-%d %H:%M:%S')
	except KeyError:
		# if that fails set the start to now - 10 days
		logger.info('no last timestamp!')
		start = end - timedelta(days=10)
	
	# get data from start to now
	data = load_alertme_data(sensor['alertme_username'], alertme_session, d, sensor['channel'], start, end)
	logger.info("loaded %d data values" % len(data))
	
	# post data to sd_store
	post_sd_store_data(sensor, data)
	logger.info("posted to sd_store\n\n")

