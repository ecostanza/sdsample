# This file is part of sdsample
#
# sdsample is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# sdsample is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with sdsample.  If not, see <http://www.gnu.org/licenses/>.

#encoding:UTF-8

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from sd_store.models import Sensor, Channel
from django.template import RequestContext

@login_required
def home_view(request):
    return redirect('calendar_example_view')
    
@login_required
def data_example_view(request):
    return render(request, 'frontend/data_example.html')

@login_required
def calendar_example_view(request):
    position = int(request.GET.get('position', 1))
    temp_channel = Channel.objects.get(name='temperature')
    sensors = Sensor.objects.filter(user=request.user, channels__in=[temp_channel])
    sensors_available = sensors.count()
    if position > sensors_available or position < 1:
        position = 1    
    sensor_idx = position-1
    sensor_to_show = sensors[sensor_idx]
    next_sensor = ((sensor_idx+1)%sensors_available)+1
    prev_sensor = ((sensor_idx-1))%sensors_available+1
    
    context = RequestContext(request, {'sensor':sensor_to_show, 'next':next_sensor, 'prev':prev_sensor})
    
    return render(request, 'frontend/calendar_example.html', context)

@login_required
def always_on_display_view(request):
    return render(request, 'frontend/always_on_display.html')