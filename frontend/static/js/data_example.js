$(function () {
    'use strict';
    var parameters;
    
    parameters = {
        url: server_url + 'sdstore/',
        start: new Date(2014, 10, 16, 14, 0),
        end: new Date(2014, 11, 2, 14, 0),
        user: true,
        channels: ['temperature',],
        sampling_interval: 60*60
    };
    
    sd_store.dataloader.load(parameters, function (data) {
    	console.log('data loaded! (by user):', data);
    	// TODO: do something with the data
    });
    
    parameters = {
            url: server_url + 'sdstore/',
            start: new Date(2014, 10, 16, 14, 0),
            end: new Date(2014, 11, 2, 14, 0),
            group: 1,
            sampling_interval: 60*60
        };
        
    sd_store.dataloader.load(parameters, function (data) {
    	console.log('data loaded! (by sensor group):', data);
    	// TODO: do something with the data
    });
        
    parameters = {
            url: server_url + 'sdstore/',
            start: new Date(2014, 10, 16, 14, 0),
            end: new Date(2014, 11, 2, 14, 0),
            sensor: 1,
            channels: ['temperature',],
            sampling_interval: 60*60
        };
        
    sd_store.dataloader.load(parameters, function (data) {
    	console.log('data loaded! (by individual sensor):', data);
    	// TODO: do something with the data
    });
        
});
