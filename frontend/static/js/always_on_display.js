$(function () {
    'use strict';
    var now = new Date(/*2014,11,13*/),
        parameters;

    now.setMinutes(0);
    now.setSeconds(0);

    window.setTimeout(function () {
        window.location.href = server_url + 'calendar_example';
    }, 5 * 60 * 1000);

    parameters = {
        url: server_url + 'sdstore/',
        start: new Date(now.getTime() - 8 * 24 * 60 * 60 * 1000),
        end: now,
        user: true, // this means "load by user"
        baseline: true,
        channels: ['energy',],
        sampling_interval: 24 * 60 * 60
    };

    //console.log(parameters.start, parameters.end);

    sd_store.dataloader.load(parameters, function (loaded_data) {
        var data = loaded_data[0],
            i;
        //console.log('data loaded! (by individual sensor):', data);
        //console.log(data.data);

        for (i=1; i<data.data.length; i += 1) {
            data.data[i].prev_value = data.data[i-1].value;
            data.data[i].t += 24 * 60 * 60 * 1000;
        }
        
        d3.select('.chart')
            .selectAll()
            .data(data.data.slice(1))
            .enter()
            .append('div')
                .attr('class', 'day')
                .html(function (d) {
                    var html = '',
                        img = 'same',
                        date = new Date(d.t),
                        now = new Date(),
                        scale_factor = 24 * 60 * 60,
                        v1 = d.prev_value,
                        v2 = d.value,
                        unit = 'W';
                    if ((v2 - v1) > (v2 * 0.02)) {
                        img = 'up';
                    } else if ((v1 - v2) > (v1 * 0.02)) {
                        img = 'down';
                    }
                    
                    if (date.getDate() === (now.getDate())) {
                         //d3.time.format("<strong>Today (%a %e)</strong>")(date);
                         html += "<h1> Today (" + d3.time.format('%d %b')(date) + ")</h1>";
                    } else {
                        html += "<h1>" + d3.time.format('%a %d %b')(date) + "</h1>";
                    }

                    html += '<img src="' + server_url + 'media/imgs/' + img + '.png" /> <br/>';
                    if (v1 > 1000.0 || v2 > 1000.0) {
                        v1 /= 1000.0;
                        v2 /= 1000.0;
                        unit = 'kW';
                    }
                    html += 'From ' + v1.toFixed(2) + ' to ' + v2.toFixed(2) + ' ' + unit;
                    return html;
                });
        $('.day').first().click(function () {document.location.reload(true);});
    });

});
