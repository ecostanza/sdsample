# This file is part of sdsample
#
# sdsample is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# sdsample is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with sdsample.  If not, see <http://www.gnu.org/licenses/>.

#encoding:UTF-8

from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('frontend.views',
    url(r'^$', 'home_view', name='home_view'),
    url(r'^data_example$', 'data_example_view', name='data_example_view'),
    url(r'^calendar$', 'calendar_example_view', name='calendar_example_view'),
    url(r'^always_on_display$', 'always_on_display_view', name='always_on_display_view'),
    #url(r'^u/(?P<username>[\w\d]+)$', 'user_view', name='user_view'),
)
