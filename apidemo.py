# This file is part of sdsample
#
# sdsample is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# sdsample is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with sdsample.  If not, see <http://www.gnu.org/licenses/>.

# encoding: utf-8
'''
This example shows how to:
- login
- create a sensor
- create a channel for this sensor
- upload and retrive data for the sensor-channel 

Create a sensor and a channel is something that should be done just one off, 
when setting up the system. These operations can also be done via the admin
web UI (but it may be tedious to do that for several sensors).

This example assumes that a user with username example and password example exists 
and a UserProfile for the user exists
These can be created from http://127.0.0.1:8000/admin/
Add the user first, then when adding the UserProfile select the 'example' user 
from the dropdown menu and leave everything else blank
'''

import requests
from datetime import datetime, timedelta
from time import mktime
import simplejson as json

sdstore_url = 'http://127.0.0.1:8000/'
uname = 'example'
upass = 'example'

### login
login_url = sdstore_url + 'accounts/login/?next=/'	

# we need to use the session feature of requests to deal with django login magic
s = requests.Session()

# get the login page to get the CSRF cookie
r = s.get(login_url)
cookies = {'sessionid': r.cookies['sessionid']}

# apart from obvious username and password we need to pass the 
# CSRF token, used by django for security
login_params = {
			'username': uname,
		  	'password': upass,
		  	'csrfmiddlewaretoken': r.cookies['csrftoken']
		  	}
r = s.post(login_url, data=login_params)
### we are now logged in


### create a sensor 
# (note that this can also be done from the admin web UI)
sensors_url = sdstore_url + 'sdstore/sensors/'

# "mac" is supposed to be mac address but formatting is not enforced 
# so it can be any text, however needs to be unique
sensor_params = {"mac": '1',
                "name": '1st sensor',
                "sensor_type": 'company X sensor'}

r = s.post(sensors_url, data=sensor_params)
# the response contains the ID of the newly created sensor
sensor_id = int(r.text)
### sensor creation done

# get a list of sensors, just to check it worked
r = s.get(sensors_url)


### add a channel, in this example temperature
# (note that this can also be done from the admin web UI)
channel_name = 'temperature'
channel_url = sdstore_url + 'sdstore/sensor/%d/%s/' % (sensor_id, channel_name)

channel_params = {
		"unit": 'degree C',
		"reading_frequency": '60' # in seconds
		}

r = s.post(channel_url, data=channel_params)
### channel added


### post single data point to the newly created channel
data_url = sdstore_url + 'sdstore/sensor/%d/%s/data/' % (sensor_id, channel_name)

# data needs to be expressed as a list of (dictionary) objects 
sensor_data = [{
			'timestamp': datetime.now().strftime('%a %b %d %H:%M:%S %Y'),
			'value': 25.0
		},]
# the list needs to be converted to json
dataString = json.dumps(sensor_data)
post_data = {"data": dataString}

r = s.post(data_url, post_data)
print r.text
### data posted, the result is the number of samples posted


### post multiple data points to the newly created channel
data_url = sdstore_url + 'sdstore/sensor/%d/%s/data/' % (sensor_id, channel_name)

# data needs to be expressed as a list of (dictionary) objects 
# NOTE: timestamps need to be distinct (for a given sensor & channel)
sensor_data = [{
			'timestamp': (datetime.now() + timedelta(minutes=1)).strftime('%a %b %d %H:%M:%S %Y'),
			'value': 25.0
		},{
			'timestamp': (datetime.now() + timedelta(minutes=2)).strftime('%a %b %d %H:%M:%S %Y'),
			'value': 25.1
		}]
# the list needs to be converted to json
dataString = json.dumps(sensor_data)
post_data = {"data": dataString}

r = s.post(data_url, post_data)
print r.text
### posting multiple data points done


### get data back 
# (this is usually done in javascript, so should not matter too much here..) 
data_params = {
'start': '2014-11-5 10:00:00',
'end': '2014-11-19 10:00:00',
'sampling_interval': '60'
}
r = s.get(data_url, params=data_params)
# should get JSON back
print r.text
### done getting data
