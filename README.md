The Sdsample Project

This project requires a number of python libraries, these are listed in the 
requirements.txt file. If planning to use Eclipse as an IDE, please install the 
libraries in a virtual environment named sdsample located in the folder ~/pve 
-- on OS X or Linux that means running (assuming virtualenv is installed):

```
mkdir -p ~/pve
virtualenv ~/pve/django4_virtualenv
```

and to activate the environment:
```
source ~/pve/django4_virtualenv/bin/activate
```

It should be possible to use requirements.txt to install everything 
through pip, by running:
```
pip install -r requirements.txt
```

Please note that it is critical to install the versions specified in the 
requirements file.  

The project also require a number of git submodules. You need to get those 
separately running:
```
git submodule init
```
and
```
git submodule update
```
 
(more information about git submodules at the bottom of this file)

Once all required libraries and submodules are installed, please follow the 
instructions below to setup the databases for the project.

Open a terminal and go to the folder where manage.py is and run:

```
python manage.py syncdb 
```

when asked, please create an admin user account.

Then run:

```
python manage.py populate_sd_store_db
python manage.py runserver
```

Now you should be able to access the site on the development server, for 
example by going to http://127.0.0.1:8000/logger
use the following credentials to login:
username: demo
password: demo
You should be able to see data from there.

---
Extra information about submodules.

For more information about git submodules, if needed, please see:
http://git-scm.com/book/en/Git-Tools-Submodules#Cloning-a-Project-with-Submodules

When you do the submodule step, you may get an error that has to do with ssh 
authentication. To solve it please  see:
https://confluence.atlassian.com/display/BITBUCKET/Set+up+SSH+for+Git

More information should also be available on these two:
http://superuser.com/questions/272465/using-multiple-ssh-public-keys
http://stackoverflow.com/questions/11567418/git-submodule-update-and-authentication